NOTE CONCERNANT LES MISE A JOUR :

Lors de la mise a jour du fichier jquery.js et minified/jquery.min.js, ajouter
les lignes ci-dessous autour du code :

 

var bab_noConflict = (typeof($) !== "undefined"); 

[.... original jquery.js or jquery.min.js ....]

if (bab_noConflict) {
	jQuery.noConflict();
}



Les fichiers dans i18n sont encodes en utf8.
Pour eviter les problemes on convertit en ascii avec des htmlentities.